type TShades = '100' | '200' | '300' | '400' | '500' | '600' | '700' | '800' | '900';
type TSeverity = 'success' | 'warning' | 'danger' | 'gray';

type TPalette = Record<'global' | 'custom', Partial<Record<TSeverity, Partial<Record<TShades, string>>>>>;

export const palette: TPalette = {
  global: {
    success: {
      100: '#CCF7EC',
      300: '#66E7C6',
      600: '#00AC80',
    },
    danger: {
      100: '#FFE0E0',
      300: '#FFA2A2',
      500: '#FF6464',
    },
    gray: {
      100: '#6B7280',
      900: '#111827',
    }
  },
  custom: {},
};