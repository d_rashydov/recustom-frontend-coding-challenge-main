import React from "react";
import type { ReactNode } from "react";
import Check from "@mui/icons-material/Check";
import { SAlert, SIconContainer, SText } from "./Notification.styles";
import type { SnackbarProps } from "@mui/material";
import { AlertTitle, Snackbar } from "@mui/material";
import { palette } from "../../modules/utils/theming/palette";
import IconBell from "../icons/IconBell";
import IconCheckRounded from "../icons/IconCheckRounded";
import IconWarning from "../icons/IconWarning";

export type TNotificationProps = {
  severity: 'success' | 'error';
  open: boolean;
  appearance?: 'card' | 'alert';
  title?: string;
  message?: string;
  onClose?: () => void;
  actions?: ReactNode[];
  children?: ReactNode;
} & SnackbarProps;

export const Notification = (props: TNotificationProps) => {
  const { severity, title, message, open, onClose, actions, children, appearance = 'alert', ...restProps } = props;
  const alertTitles = {
    success: 'Success',
    error: 'Attention',
  };

  const iconMapping = actions ? { success: <></>, error: <></> } : {
    success: (
      <SIconContainer severity="success">
        <Check sx={{ color: palette.global.success?.[600] }} fontSize="inherit" />
      </SIconContainer>
    ),
    error: (
      <SIconContainer severity="error">
        <IconBell fill={palette.global.danger?.[500]} />
      </SIconContainer>
    ),
  };

  const icons = {
    success: (
      <IconCheckRounded fill={palette.global.success?.[600]} />),
    error: (
      <IconWarning fill={palette.global.danger?.[500]} />
    ),
  }

  const isCard = appearance === 'card';
  const isInline = !Boolean(actions) && !isCard;
  const isMultiLine = Boolean(actions) && !isCard;

  return (
    <Snackbar autoHideDuration={6_000} open={open} onClose={onClose} {...restProps}>
      <SAlert
        data-testid="notification"
        onClose={onClose}
        severity={severity}
        variant={appearance === 'alert' ? 'outlined' : 'standard'}
        iconMapping={iconMapping}
        hideIcon={Boolean(actions)}
        sx={{ alignItems: actions ? 'start' : 'center' }}
      >
        {isCard && <>{children}</>}
        {isMultiLine && (
          <>
            <AlertTitle sx={{ fontWeight: '600', display: 'flex', alignItems: 'center', gap: 1 }}>
              {icons[severity]}
              {alertTitles[severity]}
            </AlertTitle>
            <SText sx={{ marginBottom: '12px' }}>{message}</SText>
            {actions?.map((action) => action)}
          </>
        )}
        {isInline && <SText>{title}</SText>}
      </SAlert>
    </Snackbar>
  );
};
