"use client";
import { styled } from "@mui/material/styles";
import { Alert, Box } from "@mui/material";
import type { AlertProps } from "@mui/material";
import { palette } from "../../modules/utils/theming/palette";

const SAlert = styled(Alert)<AlertProps & { hideIcon: boolean }>(({ severity, variant, hideIcon, theme }) => {
  const borderColors: Partial<Record<typeof severity & string, string | undefined>> = {
    success: palette.global.success?.[300],
    error: palette.global.danger?.[300]
  };
  const colors: Partial<Record<typeof severity & string, string | undefined>> = {
    success: palette.global.success?.[600],
    error: palette.global.danger?.[500]
  };

  return {
    backgroundColor: theme.palette.common.white,
    width: '100%',
    alignItems: 'center',
    borderRadius: '6px',
    '&.MuiAlert-standard': {
      boxShadow: variant === 'standard' ? '1px 1px 6px 1px hsla(0, 0%, 0%, 0.08)' : 'initial'
    },
    '&.MuiPaper-root': {
      borderColor: borderColors[severity as keyof typeof borderColors],
      color: variant ? palette.global.gray?.[900] : colors[severity as keyof typeof colors],
      '.MuiAlert-icon': {
        display: hideIcon ? 'none' : 'initial',
      }
    },
  }
});

const SIconContainer = styled(Box)<AlertProps>(({ severity, theme }) => {
  const backgroundColors: Partial<Record<typeof severity & string, string | undefined>> = {
    success: palette.global.success?.[100],
    error: palette.global.danger?.[100]
  };

  return {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '7px',
    padding: theme.spacing(1),
    '&.MuiBox-root': {
      backgroundColor: backgroundColors[severity as keyof typeof backgroundColors],
    },
  }
});

const SText = styled(Box)<AlertProps>(({ severity }) => {
  const colors: Partial<Record<typeof severity & string, string | undefined>> = {
    success: palette.global.success?.[600],
    error: palette.global.danger?.[500]
  };

  return {
    '&.MuiTypography-root': {
      backgroundColor: colors[severity as keyof typeof colors],
    },
  }
});

const SContainer = styled(Box)<AlertProps>(({ severity }) => {
  const borderColors: Partial<Record<typeof severity & string, string | undefined>> = {
    success: palette.global.success?.[300],
    error: palette.global.danger?.[300]
  };

  return {
    '&.MuiBox-root': {
      borderColor: borderColors[severity as keyof typeof borderColors],
    },
  }
});

export { SContainer, SAlert, SIconContainer, SText };
