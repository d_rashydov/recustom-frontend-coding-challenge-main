import { describe, it, expect } from "vitest";
import { render } from '@testing-library/react';
import { Notification, TNotificationProps } from './Notification';
import { useState } from "react";
import { Button } from "@mui/material";
import { userEvent } from "@storybook/test";

const Wrapper = (props: Partial<TNotificationProps>) => {
  const [open, setOpen] = useState<boolean>(false);

  const onOpen = () => setOpen(true);
  const onClose = () => setOpen(false);

  return (
    <>
      <Button data-testid="trigger" onClick={onOpen}>Open</Button>
      {open && (
        <Notification
          severity="success"
          title="The action that you have done was a success! Well done"
          open={open}
          onClose={onClose}
          anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
          {...props}
        />
      )}
    </>
  );
};

describe('Notification', () => {
  afterEach(() => {
    vitest.useRealTimers();
  });

  it('renders', async () => {
    const { getByTestId, getByRole } = render(<Wrapper />);
    const trigger = getByTestId('trigger');
    await userEvent.click(trigger);
    const notification = getByRole('alert');
    expect(notification).toBeDefined();
  });

  it('gets hidden on close', async () => {
    const { getByTestId, container, getByLabelText, getByRole } = render(<Wrapper />);
    const trigger = getByTestId('trigger');
    await userEvent.click(trigger);
    const notification = getByRole('alert');
    expect(container).toContain(notification);
    const close = getByLabelText('Close');
    await userEvent.click(close);
    expect(container).not.toContain(notification);
  });

  it('has success severity', async () => {
    const { getByTestId, rerender, getByRole } = render(<Wrapper severity="success" />);
    const trigger = getByTestId('trigger');
    await userEvent.click(trigger);
    const success = getByRole('alert');
    expect(success.classList).toContain('MuiAlert-outlinedSuccess');
    rerender(<Wrapper severity="error" />)
    const error = getByRole('alert');
    expect(error.classList).toContain('MuiAlert-outlinedError');
  });

  it('has appearance of type card', async () => {
    const { getByTestId, getByRole } = render(<Wrapper appearance="card" children={<div data-testid="custom">Test</div>} />);
    const trigger = getByTestId('trigger');
    await userEvent.click(trigger);
    const alert = getByRole('alert');
    const custom = getByTestId('custom');
    expect(custom).toBeDefined();
  });
})
