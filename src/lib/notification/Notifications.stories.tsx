import type { Meta, StoryObj } from '@storybook/react';
import { ReactElement, useState } from 'react';
import { Avatar, Box, Button, Typography } from '@mui/material';

import { Notification } from './Notification';
import type { TNotificationProps } from './Notification';

const Wrapper = (props: TNotificationProps) => {
  const [open, setOpen] = useState<boolean>(false);

  const onOpen = () => setOpen(true);
  const onClose = () => setOpen(false);

  return (
    <>
      <Button onClick={onOpen}>Open</Button>
      <Notification
        {...props}
        open={open}
        onClose={onClose}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      />
    </>
  );
};

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories#default-export
const meta = {
  title: 'Example/Notification',
  component: Wrapper,
  parameters: {
    // Optional parameter to center the component in the Canvas. More info: https://storybook.js.org/docs/configure/story-layout
    layout: 'padded',
  },
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ['autodocs'],
  // More on argTypes: https://storybook.js.org/docs/api/argtypes
} satisfies Meta<typeof Wrapper>;

export default meta;
type Story = StoryObj<typeof meta>;

// More on writing stories with args: https://storybook.js.org/docs/writing-stories/args
export const Success: Story = {
  args: {
    open: true,
    severity: 'success',
    title: 'The action that you have done was a success! Well done'
  },
};

const Action = ({ severity }: Pick<TNotificationProps, 'severity'>) => {
  const colors = {
    success: '#00AC80',
    error: '#FF6464',
  };

  return (
    <Button sx={{
      borderRadius: '26px',
      height: '26px',
      backgroundColor: colors[severity],
      border: '1px solid #000'
    }}>
      <Typography fontSize={12} color="#fff">Take action</Typography>
    </Button>
  )
};

const Card = () => {
  return (
    <Box sx={{ display: 'flex', gap: 1 }}>
      <Avatar sx={{ width: 32, height: 32 }}>
        <Typography fontSize={12}>BG</Typography>
      </Avatar>
      <Box sx={{ flexDirection: 'column' }}>
        <Typography fontSize={14} fontWeight={600} color="#111928">Bonnie Green</Typography>
        <Typography fontSize={14} color="#6B7280">
          Hi Neil, thanks for sharing your thoughts regardingFlowbite.
        </Typography>
        <Button sx={{
          borderRadius: '26px',
          height: '26px',
          backgroundColor: '#FFFF00',
          border: '1px solid #000',
          color: '#000',
          marginTop: '12px'
        }}>
          <Typography fontSize={12}>Button text</Typography>
        </Button>
      </Box>
    </Box>
  )
};

export const SuccessWithActions: Story = {
  args: {
    open: true,
    severity: 'success',
    message: 'Well done, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content. Be sure to use margin utilities to keep things nice and tidy.',
    actions: [<Action severity="success" />],
  },
};

export const ErrorWithActions: Story = {
  args: {
    open: true,
    severity: 'error',
    message: 'Oh snap, you successfully read this important alert message.This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.Be sure to use margin utilities to keep things nice and tidy.Well done, you successfully read this important alert message.This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.Be sure to use margin utilities to keep things nice and tidy.',
    actions: [<Action severity="error" />],
  },
};

export const Error: Story = {
  args: {
    open: true,
    severity: 'error',
    title: 'The file flowbite-figma-pro was permanently deleted.'
  },
};

export const NotificationCard: Story = {
  args: {
    open: true,
    appearance: 'card',
    severity: 'success',
    message: 'Well done, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content. Be sure to use margin utilities to keep things nice and tidy.',
    actions: [<Action severity="success" />],
    children: <Card />
  },
};
